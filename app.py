#!/usr/bin/python

server="jakabylapogoda.herokuapp.com"
token=""

class Req:
	status_code=201

import bme280
import Adafruit_DHT as dht22
import requests
import time

sensor = dht22.DHT22
pin=4
counter=0
req=Req()

while req.status_code==201 or req==0:
	counter=counter+1

	temperature1,pressure,humidity_null = bme280.readBME280All()
	humidity,temperature2 = dht22.read_retry(sensor, pin)

	temperature = round((temperature1+temperature2)/2, 2)
	pressure = round(pressure, 2)
	humidity = round(humidity, 2)

	print counter, ">> Time: ", time.ctime(), " | Temperature: ", temperature, "C | Pressure: ", pressure, "hPa | Humidity: ", humidity, "%"

	url="http://"+server+"/api/new/"+str(token)+"/"+str(temperature)+","+str(pressure)+","+str(humidity)

	req=requests.get(url)

	if req.status_code==201:
		print counter, '>> Response: OK'
	else:
		print counter, '>> Response: ERROR', req.status_code
	time.sleep(900)
